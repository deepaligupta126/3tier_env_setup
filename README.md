# 3tier_env_setup

## Azure based 3 tier env setup for a basic application deployments


- **Virtual Machine Linux** : Vurtual Machine is required for application deployment 
- **Storage account**: Storage account is required to store data related to application
- **Azure Key Vault** : keyvault is required to store protected data like passwords.

**This project will create a basic 3 tier env with azure terraform code .**

- [ ] IaC tool : terraform
- [ ] Deployment Tool : Gitlab
- [ ] Resources provisoned from : Azure


